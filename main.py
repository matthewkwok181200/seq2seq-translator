#open dataset and split into eng and chin
#########################################

with open('yue.txt') as f:
    lines = f.readlines()

df = lines

df1 = []
for x in df:
  df1.append(x.split("\t"))


eng_ds = []
chin_ds = []

for data in df1:
  eng_sen = []
  eng_sen.append(data[0].split())
  eng_ds.append(eng_sen)

  chin_sen = []
  for char in data[1]:
    chin_sen.append(char)
  chin_sen.append("<end>")
  chin_sen.insert(0, "<start>")

  chin_ds.append(chin_sen)



# getting max encoder decoder length
#########################################


eng_dic = {}
chin_dic = {}

x = 0
y = 0

for eng_sentence in eng_ds:
  for eng_char in eng_sentence[0]:
    if eng_char not in eng_dic.keys():
      eng_dic[eng_char] = x
      x += 1

for chin_sentence in chin_ds:
  for chin_char in chin_sentence:
    if chin_char not in chin_dic.keys():
      chin_dic[chin_char] = y
      y += 1

ha = []
for text in eng_ds:
  ha.append(text[0])

max_encoder_seq_length = max([len(text) for text in ha])
max_decoder_seq_length = max([len(text) for text in chin_ds])


#one hot encode dataset
#########################################


import numpy as np

one_hot_eng = np.zeros((len(eng_ds), max_encoder_seq_length,len(eng_dic)))
one_hot_chin = np.zeros((len(chin_ds),max_decoder_seq_length,len(chin_dic)))
one_hot_chin_target = np.zeros((len(chin_ds),max_decoder_seq_length,len(chin_dic)))

for index, eng_sentence in enumerate(eng_ds):
  for eng_char in eng_sentence:
    for x_index, char in enumerate(eng_char):
      h = eng_dic[char]
      one_hot_eng[index][x_index][h] = 1

for index, chin_sentence in enumerate(chin_ds):
  for x_index, char in enumerate(chin_sentence):
    h = chin_dic[char]
    one_hot_chin[index][x_index][h] = 1

for index, chin_sentence in enumerate(chin_ds):
  for x_index, char in enumerate(chin_sentence):
    h = chin_dic[char]
    one_hot_chin_target[index][x_index-1][h] = 1


#encoder decoder sequence
#########################################


from keras.models import Model
from keras.layers import Input, LSTM, Dense

encoder_model = Input(shape = (None,len(one_hot_eng[0][0])))
_, h, c = LSTM(256, return_state = True)(encoder_model)
encoder_states = [h,c]

decoder_model = Input(shape=(None, len(one_hot_chin[0][0])))
decoder_output = LSTM(256, return_sequences=True)(decoder_model,  initial_state = encoder_states)
decoder_softmax = Dense(len(one_hot_chin[0][0]), activation='softmax')(decoder_output)

model = Model ([encoder_model, decoder_model], decoder_softmax)
model.compile(optimizer='rmsprop',loss='categorical_crossentropy',metrics=['accuracy'])

model.fit(x=[one_hot_eng, one_hot_chin], y=one_hot_chin_target,
          batch_size = 64, epochs=100)

#prediction
#########################################

encoder_input = Model(inputs = encoder_model, outputs = encoder_states)

decoder_states_h = Input(shape = (256))
decoder_states_c = Input(shape = (256))
decoder_input_states = [decoder_states_h,decoder_states_c]


decoder_model = Input(shape=(None, len(one_hot_chin[0][0])))
decoder_output, h, c = LSTM(256, return_sequences=True, return_state = True)(decoder_model, initial_state = decoder_input_states)
decoder_output_states = [h,c]

decoder_softmax = Dense(len(one_hot_chin[0][0]), activation='softmax')(decoder_output)

decoder_output = Model (inputs = [decoder_model]+ decoder_input_states, outputs = [decoder_softmax]+ decoder_output_states)
seed = "What a nice day"

one_hot_seed = np.zeros((1, 1,len(eng_dic)))

for index, x in enumerate(seed.split()):
  one_hot_seed[0][0][eng_dic[x]] = 1
decoder_input_states = encoder_input.predict(one_hot_seed)


one_hot_output = np.zeros((1,1,len(chin_dic)))
one_hot_output[0][0][chin_dic["<start>"]] = 1

line = ""
hex = True

output_token, h, c = decoder_output.predict([one_hot_output] + decoder_input_states)
decoder_input_states = [h,c]
answer = np.argmax(output_token)
one_hot_output = np.zeros((1,1,len(chin_dic)))
one_hot_output[0][0][answer] = 1

while answer != 5:
  output_token, h, c = decoder_output.predict([one_hot_output] + decoder_input_states)
  decoder_input_states = [h,c]
  answer = np.argmax(output_token)
  one_hot_output = np.zeros((1,1,len(chin_dic)))
  one_hot_output[0][0][answer] = 1

  line += list(chin_dic.keys())[list(chin_dic.values()).index(answer)]
  print(line)



